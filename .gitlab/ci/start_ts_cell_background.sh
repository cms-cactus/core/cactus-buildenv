#!/usr/bin/env bash

export PID_DIR=/var/run
export LOCK_DIR=/var/lock
export CORE_DIR=/tmp
export XDAQ_ZONE=trigger
export XDAQ_EXTERN_LIBRARY_PATH=/opt/xdaq/lib
export XDAQ_ROOT=/opt/xdaq
export XDAQ_SETUP_ROOT=/opt/xdaq/share
export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs
export LD_LIBRARY_PATH=/opt/xdaq/lib:/opt/cactus/lib
export XDAQ_USER=root
export XDAQ_LOG=/var/log/trigger-cell.log

export XDAQ_LOG=${PWD}/output.log

touch ${XDAQ_LOG}

# In the configure file remove all the TCDS applications (important to run the tcds cell). Needs to run bevor the next sed command which replaces the urls.
sed -i '/<xc:Context url=.http:\/\/tcds-control-.*/,/<.xc:Context>/d' ${XDAQ_CONFIG}

sed -i 's%xc:Context url=\"http://.*:\([0-9]*\)\"%xc:Context url=\"http://'"${HOSTNAME}"':\1\"%g' ${XDAQ_CONFIG}

# "In the configure file, remove the LAS Application.
sed -i '/<xc:Context.*kvm-s3562-1-ip151-102.cms:9945.*/,/<.xc:Context>/d' ${XDAQ_CONFIG}

# In the profile file, remove the trace probe application.
sed -i "/<xp:Application.*class=.tracer/,/<\/xp:Application>/d" ${XDAQ_PROFILE}

echo STARTING CELL
/opt/xdaq/bin/xdaq.exe -h ${HOSTNAME} -p ${XDAQ_PORT} \
  -e ${XDAQ_PROFILE} \
  -c ${XDAQ_CONFIG} \
  -z ${XDAQ_ZONE} \
  -u file.append:${XDAQ_LOG} &

# waits for the cell to start up
sleep 20s
