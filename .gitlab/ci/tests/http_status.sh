#!/usr/bin/env bash

# get http status
http_status=$(curl -o /dev/null -L --silent --write-out '%{http_code}' ${HOSTNAME}:${XDAQ_PORT}/urn:xdaq-application:lid=13/)

# returns error if fail
if [ ${http_status} -eq 200 ]; then
  echo "Cell webpage works as expected and returned HTTP status 200."
  exit 0
else
  >&2 echo "Cell webpage doesn't work correctly. It returned the HTTP status ${http_status}. Verify that the cell successfully started up in the following logs\n\n"
  cat ${XDAQ_LOG}
  exit 1
fi
