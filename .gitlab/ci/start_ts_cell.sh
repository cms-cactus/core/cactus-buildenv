#!/usr/bin/env bash
# This script replaces the static hostname in the configure files with one that will be used when the built container is started with: docker run --hostname ...
# After that this script starts the XDAQ cell with this hostname.

export XDAQ_HOSTNAME=${HOSTNAME}

# In the configure file, replace the urls with the hostname. The env needs to be in double quotes.
sed -i 's%xc:Context url=\"http://.*:\([0-9]*\)\"%xc:Context url=\"http://'"${XDAQ_HOSTNAME}"':\1\"%g' ${XDAQ_CONFIG}

echo "STARTING XDAQ SWATCH CELL"
/opt/xdaq/bin/xdaq.exe -h ${XDAQ_HOSTNAME} -p ${XDAQ_PORT} -e ${XDAQ_PROFILE} -c ${XDAQ_CONFIG} -z ${XDAQ_ZONE}
