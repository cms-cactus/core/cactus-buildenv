FROM cern/cc7-base:20200715-1.x86_64
LABEL maintainer="Cactus Role Account <cactus@cern.ch>"

ARG BASE_OS=cc7

ARG BUILD_UTILS_VERSION=0.2.3

ARG IPBUS_VERSION=2.8
ARG IPBUS_PATCH_VERSION=2-1

COPY cactus-build-utils.noarch.repo /etc/yum.repos.d/
COPY ipbus-sw-${IPBUS_VERSION}.${BASE_OS}.x86_64.repo /etc/yum.repos.d/
COPY cactus-prometheus-cpp-1.2.cc7.x86_64.repo /etc/yum.repos.d/

RUN \
  sed -i 's#http://linuxsoft.cern.ch/epel/7/#http://linuxsoft.cern.ch/internal/archive/epel/7/#g' /etc/yum.repos.d/epel.repo && \
  yum install -y \
    --setopt=skip_missing_names_on_install=False \
    log4cplus \
    createrepo \
    bzip2-devel \
    zlib-devel \
    ncurses-devel \
    python3-devel \
    curl curl-devel \
    graphviz graphviz-devel \
    wxPython \
    e2fsprogs-devel \
    libuuid-devel \
    qt qt-devel \
    libusb libusb-devel \
    gd-devel \
    xsd valgrind \
    vim \
    jsoncpp-devel \
    readline-devel \
    centos-release-scl devtoolset-8-gcc devtoolset-8-gcc-c++ \
    cactuscore-build-utils-$BUILD_UTILS_VERSION \
    # from yum groupinstall uhal
    cactuscore-uhal-{uhal,grammars,log,python36-gui,python36,tests,tools}-$IPBUS_VERSION.$IPBUS_PATCH_VERSION* \
    prometheus-cpp \
	 && \
  yum groupinstall -y "Development Tools" && \
  yum clean all

ENTRYPOINT ["/usr/bin/scl", "enable", "devtoolset-8", "--", "/bin/bash", "-c"]
CMD ["/bin/bash"]
