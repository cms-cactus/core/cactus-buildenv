ARG CI_COMMIT_REF_NAME
ARG CI_COMMIT_SHORT_SHA=latest

FROM gitlab-registry.cern.ch/cms-cactus/core/cactus-buildenv/scratch:${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}
LABEL maintainer="Cactus Role Account <cactus@cern.ch>"

# this must match the OS of the scratch base image
# unfortunately, we can't inherit ARG values
ARG BASE_OS=cc7
ARG XDAQ_MAJOR_VERSION=15

ARG AMC13_VERSION=1.3
ARG AMC13_PATCH_VERSION=1-0

ARG MP7_VERSION=3.0
ARG MP7_PATCH_VERSION=0-0

COPY xdaq${XDAQ_MAJOR_VERSION}.${BASE_OS}.x86_64.repo /etc/yum.repos.d/
COPY cactus-mp7-${MP7_VERSION}.${BASE_OS}.x86_64.repo /etc/yum.repos.d/
COPY cactus-amc13-${AMC13_VERSION}.${BASE_OS}.x86_64.repo /etc/yum.repos.d/

# create folders and copy files for tests
COPY .gitlab/ci /opt/cactus/test/

RUN \
  yum groupinstall -y \
    --setopt=skip_missing_names_on_install=False \
    cmsos_core \
    cmsos_core_debuginfo \
    cmsos_worksuite \
    cmsos_worksuite_debuginfo \
    # we're not intalling JAVA just yet
    --exclude cmsos-worksuite-dipbridge \
    --exclude cmsos-worksuite-amc13controller \
   && \
  yum install -y \
    --setopt=skip_missing_names_on_install=False \
    # from yum groupinstall amc13
    cactusboards-amc13-{amc13,tools,python}-$AMC13_VERSION.$AMC13_PATCH_VERSION* \
    # from yum groupinstall mp7
    cactusboards-mp7-{mp7,pycomp7,tests}-$MP7_VERSION.$MP7_PATCH_VERSION* \
   && \
  yum clean all

CMD ["/bin/bash"]
